module.exports = function(search) {
  var configEnv = {};
  for (var envname in process.env) {
    var subList = envname.split('_');
    const root = subList.shift();
    if (root !== search) {
      continue;
    }
    resolve(subList,configEnv,envname);
   
  };
  return configEnv;
};
function resolve(subList,configEnv,envname,key){
  if(subList.length == 0){
    configEnv[key] = process.env[envname];
    return;
  }
  currentKey = subList.shift().toLowerCase();
  if(!key){
    return resolve(subList,configEnv,envname,currentKey);
  }

  if(!configEnv[key]){
    configEnv[key] = {};
  }
  return resolve(subList,configEnv[key],envname,currentKey);
}