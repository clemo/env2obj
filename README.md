# env2obj

> a small module to convert envirument variables to a object

sample:

```js
EXAMPLE_FOO=hello EXAMPLE_BAR=World node
var config =  require ('env2obj'); 
config('EXAMPLE'); //{ foo: 'hello', bar: 'World' }
```
