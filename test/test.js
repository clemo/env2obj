const { expect } = require('chai');
const config = require('../env2obj');

//set test values
process.env.W_X = 'A';
process.env.W_Y = 'B';
process.env.W_A_B_C_D = 'C';

it('should get correct ENV', () => {
  const c = config('W');
  expect(c).have.property('x', 'A');
  expect(c).have.property('y', 'B');
  expect(c)
    .have.property('a')
    .property('b')
    .property('c')
    .property('d', 'C');
});
